<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/crud','CrudController@index')->name('crud');
Route::get('/crud/add','CrudController@add')->name('cr.t');
Route::get('crud/edit','CrudController@edit')->name('cr.e');
Route::post('crud/save','CrudController@save')->name('cr.save');
Route::delete('crud/delete/{id}','CrudController@delete')->name('cr.delete');
Route::get('crud/{id}/edit','CrudController@edit')->name('cr.edit');
Route::patch('crud/{id}','CrudController@update')->name('cr.update');
