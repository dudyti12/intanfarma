<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataObatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_obat', function (Blueprint $table) {
            $table->id();
            $table->string('kode_obat',25);
            $table->string('nama_obat',100);
            $table->integer('stok_obat');
            $table->string('bentuk_obat',10);
            $table->string('konsumen_obat');
            $table->string('manfaat_obat',100);
            $table->integer('harga_obat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_obat');
    }
}
